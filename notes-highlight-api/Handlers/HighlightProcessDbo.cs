using notes_highlight_api.Handlers.DAL;

namespace notes_highlight_api.Handlers;

public class HighlightProcessDbo
{
    [Column("id", YdbType.Guid)] public Guid Id { get; set; }
    [Column("status", YdbType.Int)] public int Status { get; set; }

    [Column("base_image_link", YdbType.String)]
    public required string BaseImageLink { get; set; }

    [Column("ready_image_link", YdbType.OptionalString)]
    public string? ReadyImageLink { get; set; }

    [Column("timestamp", YdbType.Timestamp)]
    public long Timestamp { get; set; }

    private void Bare()
    {
        new HighlightProcessDbo(){BaseImageLink = ""}.GetProperties();
    }
}
