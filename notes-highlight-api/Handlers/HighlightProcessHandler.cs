using System.Text;
using notes_highlight_api.Common;
using notes_highlight_api.Handlers.DAL;
using Ydb.Sdk.Value;

namespace notes_highlight_api.Handlers;

public class HighlightProcessHandler(
    IYdbScheme scheme,
    YdbSerializerHelper<HighlightProcessDbo> serializerHelper,
    IYdbClient client)
    : BaseHandler<HighlightProcessDbo>(scheme, serializerHelper, client), IHighlightProcessHandler
{
    public async Task<HighlightProcessDbo?> GetStatus(Guid id)
    {
        var rows = await _client.ExecuteFind($@"
            DECLARE $id AS string;

            {Select}
            FROM {_scheme.Statuses}
            WHERE id = $id
        ", new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
        });

        var row = rows?.FirstOrDefault();
        return row is null ? null : FromYRow(row);
    }

    public async Task UpdateStatus(HighlightProcessDbo dbo)
    {
        dbo.Timestamp = DateTime.UtcNow.Ticks;
        var ydb = _serializerHelper.GenerateUpsert(dbo, _scheme.Statuses);
        await _client.ExecuteModify(ydb.Yql, ydb.Item2);
    }

    private static HighlightProcessDbo FromYRow(ResultSet.Row row)
    {
        var baseLink = row["base_image_link"].GetString();
        var readyLink = row["ready_image_link"].GetOptionalString();
        return new HighlightProcessDbo()
        {
            Id = row["id"].GetString().GetGuid(),
            BaseImageLink = Encoding.Default.GetString(baseLink),
            ReadyImageLink = readyLink is null ? null : Encoding.Default.GetString(readyLink),
            Timestamp = row["timestamp"].GetTimestamp().Ticks,
            Status = row["status"].GetInt32(),
        };
    }

    private const string Select = "select id, timestamp, status, base_image_link, ready_image_link";
}