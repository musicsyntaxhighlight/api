namespace notes_highlight_api.Handlers;

public interface IHighlightProcessHandler
{
    public Task<HighlightProcessDbo?> GetStatus(Guid id);
    public Task UpdateStatus(HighlightProcessDbo dbo);
}