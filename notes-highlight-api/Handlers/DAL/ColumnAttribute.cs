namespace notes_highlight_api.Handlers.DAL;

public class ColumnAttribute(string name, YdbType type) : Attribute
{
    public string Name = name;
    public YdbType Type = type;
}
