namespace notes_highlight_api.Handlers.DAL;

public class BaseHandler<T>(IYdbScheme scheme, YdbSerializerHelper<T> serializerHelper, IYdbClient client)
{
    protected readonly IYdbScheme _scheme = scheme;
    protected readonly YdbSerializerHelper<T> _serializerHelper = serializerHelper;
    protected readonly IYdbClient _client = client;
}