namespace notes_highlight_api.Handlers.DAL;

public enum YdbType
{
    Unknown,
    String,
    OptionalString,
    Guid,
    OptionalGuid,
    Int,
    OptionalInt,
    DateTime,
    OptionalDateTime,
    Bytes,
    Timestamp
}