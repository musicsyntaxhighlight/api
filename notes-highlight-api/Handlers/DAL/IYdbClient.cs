using Ydb.Sdk.Value;

namespace notes_highlight_api.Handlers.DAL;

public interface IYdbClient
{
    Task ExecuteScheme(string query);
    
    Task<IEnumerable<ResultSet.Row>?> ExecuteFind(
        string query, Dictionary<string, YdbValue> parameters);

    Task ExecuteModify(string query, Dictionary<string, YdbValue> parameters);
}