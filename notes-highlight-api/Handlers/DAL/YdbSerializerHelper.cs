using System.Text;
using notes_highlight_api.Common;
using Ydb.Sdk.Value;

namespace notes_highlight_api.Handlers.DAL;

public class YdbSerializerHelper<T>
{
    public (string Yql, Dictionary<string, YdbValue>) GenerateUpdate(T model, string tableName)
        => GenerateInsertion(model, tableName, "UPSERT");

    public (string Yql, Dictionary<string, YdbValue>) GenerateUpsert(T model, string tableName)
        => GenerateInsertion(model, tableName, "UPSERT");

    public (string Yql, Dictionary<string, YdbValue>) GenerateInsert(T model, string tableName)
        => GenerateInsertion(model, tableName, "INSERT");
    

    private (string Yql, Dictionary<string, YdbValue>) GenerateInsertion(T model, string tableName, string keyWord)
    {
        var properties = model
                         .GetProperties()
                         .Where(property => property
                                            .Value
                                            .Attributes
                                            .OfType<ColumnAttribute>()
                                            .Any())
                         .Select(property
                             => (property.Value.Attributes.OfType<ColumnAttribute>().Single().Name,
                                 property.Value.TryGetValue(model, out var value),
                                 value, property.Value.Attributes.OfType<ColumnAttribute>().Single().Type))
                         .ToArray();

        var declare = new StringBuilder();
        var ydbValues = new Dictionary<string, YdbValue>();
        foreach (var (name, _, value, type) in properties)
        {
            var ydbValue = typesMapping[type](value);
            declare.Append($"DECLARE ${name} as {typesString[type]};\n");
            ydbValues[$"${name}"] = ydbValue;
        }

        declare.Append($"{keyWord} INTO {tableName} ( {string.Join(", ", properties.Select(x => x.Name))} )\n");
        declare.Append($"VALUES ( {string.Join(", ", properties.Select(x => "$" + x.Name))} );\n");


        return (declare.ToString(), ydbValues);
    }

    private Dictionary<YdbType, Func<object, YdbValue>> typesMapping = new()
    {
        {YdbType.String, o => YdbValue.MakeString(((string) o).AsBytes())},
        {YdbType.OptionalString, o => YdbValue.MakeOptionalString(((string?) o).AsNullableBytes())},
        {YdbType.Bytes, o => YdbValue.MakeOptionalString((byte[]?)o)},
        {YdbType.Guid, o => YdbValue.MakeString(((Guid) o).GetStringView())},
        {YdbType.DateTime, o => YdbValue.MakeDatetime((DateTime) o)},
        {YdbType.OptionalDateTime, o => YdbValue.MakeOptionalDatetime((DateTime?) o)},
        {YdbType.Int, o => YdbValue.MakeInt32((int) o)},
        {YdbType.OptionalInt, o => YdbValue.MakeOptionalInt32((int?) o)},
        {YdbType.OptionalGuid, o => YdbValue.MakeOptionalString(((Guid?) o)?.GetStringView())},
        {YdbType.Timestamp, o => YdbValue.MakeTimestamp(new DateTime(((long?) o).Value))}
    };

    private Dictionary<YdbType, string> typesString = new()
    {
        {YdbType.String, "string"},
        {YdbType.OptionalString, "string?"},
        {YdbType.Guid, "string"},
        {YdbType.DateTime, "datetime"},
        {YdbType.OptionalDateTime, "datetime?"},
        {YdbType.Int, "int32"},
        {YdbType.OptionalInt, "int32?"},
        {YdbType.OptionalGuid, "string?"},
        {YdbType.Bytes, "bytes?"},
        {YdbType.Timestamp, "timestamp"}
    };
}