using AspNetCore.Yandex.ObjectStorage;

namespace notes_highlight_api.Handlers.DAL;

public interface IYandexStorageFabric
{
    public YandexStorageService Create(Configuration configuration);
}