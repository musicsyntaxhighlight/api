using AspNetCore.Yandex.ObjectStorage.Configuration;

namespace notes_highlight_api.Handlers.DAL;

public class Configuration
{
    public string YdbEndpoint => Environment.GetEnvironmentVariable(nameof(YdbEndpoint))!;
    public string YdbPath => Environment.GetEnvironmentVariable(nameof(YdbPath))!;
    public string? IamTokenPath => Environment.GetEnvironmentVariable(nameof(IamTokenPath))!;
    public string QueueUrl => Environment.GetEnvironmentVariable(nameof(QueueUrl))!;

    public YandexStorageOptions YandexStorageOptions => new()
    {
        BucketName = Environment.GetEnvironmentVariable("ImageBucketName"),
        Location = "ru-central1-b",
        Endpoint = "storage.yandexcloud.net",
        AccessKey = Environment.GetEnvironmentVariable("AWS_ACCESS_KEY_ID"),
        SecretKey = Environment.GetEnvironmentVariable("AWS_SECRET_ACCESS_KEY")
    };
}