using AspNetCore.Yandex.ObjectStorage;

namespace notes_highlight_api.Handlers.DAL;

public class YandexStorageFabric : IYandexStorageFabric
{
    public YandexStorageService Create(Configuration configuration)
    {
        return new YandexStorageService(configuration.YandexStorageOptions);
    }
}