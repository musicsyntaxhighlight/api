using Amazon.Lambda.AspNetCoreServer;

namespace notes_highlight_api
{
    public class YandexFunction : YandexGatewayProxyFunction
    {
        protected override void Init(IWebHostBuilder builder)
        {
            builder.UseStartup<Startup>();
        }
    }
}