namespace notes_highlight_api.Common;

public static class List
{
    public static List<T> From<T>(params T[] values)
    {
        return values.ToList();
    }

    public static T[][] Transpose<T>(this T[] array)
    {
        return array.Select(x => new[] {x}).ToArray();
    }

    public static async Task<IEnumerable<T>> WhenAll<T>(this IEnumerable<Task<T>> tasks)
    {
        var list = new List<T>();
        foreach (var task in tasks)
        {
            list.Add(await task);
        }

        return list;
    }
}