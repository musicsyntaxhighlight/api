using System.Text;

namespace notes_highlight_api.Common;

public static class ByteArrayExtensions
{
    public static Guid AsGuid(this byte[] bytes)
    {
        return Guid.Parse(Encoding.Default.GetString(bytes));
    }

    public static string AsString(this byte[] bytes)
    {
        return Encoding.Default.GetString(bytes);
    }

    public static string? AsNullableString(this byte[]? bytes)
    {
        return bytes is null ? null : Encoding.Default.GetString(bytes);
    }

    public static byte[] AsBytes(this string bytes)
    {
        return Encoding.Default.GetBytes(bytes);
    }

    public static byte[]? AsNullableBytes(this string? bytes)
    {
        return bytes is null ? null : Encoding.Default.GetBytes(bytes);
    }
}