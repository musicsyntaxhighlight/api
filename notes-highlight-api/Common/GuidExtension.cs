using System.Text;

namespace notes_highlight_api.Common;

public static class GuidExtension
{
    public static (ulong, ulong) AsNumbers(this Guid guid)
    {
        var guidBytes = guid.ToByteArray();

        var firstUlong = BitConverter.ToUInt64(guidBytes, 0);
        var secondUlong = BitConverter.ToUInt64(guidBytes, 8);

        if (!BitConverter.IsLittleEndian)
            return (firstUlong, secondUlong);

        firstUlong = firstUlong.ReverseBytes();
        secondUlong = secondUlong.ReverseBytes();

        return (firstUlong, secondUlong);
    }

    public static byte[] GetStringView(this Guid guid)
    {
        return Encoding.Default.GetBytes(guid.ToString());
    }

    public static Guid GetGuid(this byte[] bytes)
    {
        return Guid.Parse(Encoding.Default.GetString(bytes));
    }

    public static Guid AsGuid(this (ulong, ulong) longs)
    {
        var firstUlong = longs.Item1; // первый ulong
        var secondUlong = longs.Item2; // второй ulong

        var bytes = new byte[16];

        for (var i = 0; i < 8; i++)
        {
            bytes[i] = (byte) (firstUlong >> ((7 - i) * 8));
        }

        for (var i = 8; i < 16; i++)
        {
            bytes[i] = (byte) (secondUlong >> ((15 - i) * 8));
        }

        return new Guid(bytes);
    }

    public static Guid? AsGuid(this (ulong?, ulong?)? longs)
    {
        if (longs?.Item1 is null || longs.Value.Item2 is null) return null;
        return (longs.Value.Item1.Value, longs.Value.Item2.Value).AsGuid();
    }

    public static Guid? AsGuid(this (ulong?, ulong?) longs)
    {
        if (longs.Item1 is null || longs.Item2 is null) return null;
        return (longs.Item1.Value, longs.Item2.Value).AsGuid();
    }

    public static ulong ReverseBytes(this ulong value)
    {
        return ((value & 0x00000000000000FF) << 56) |
               ((value & 0x000000000000FF00) << 40) |
               ((value & 0x0000000000FF0000) << 24) |
               ((value & 0x00000000FF000000) << 8) |
               ((value & 0x000000FF00000000) >> 8) |
               ((value & 0x0000FF0000000000) >> 24) |
               ((value & 0x00FF000000000000) >> 40) |
               ((value & 0xFF00000000000000) >> 56);
    }
}