namespace notes_highlight_api.Common.Results;

public enum DefaultErrors
{
    NotFound = 404,
    Conflict = 409
}