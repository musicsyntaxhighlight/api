namespace notes_highlight_api.Common.Results;

public readonly record struct Result<TValue, TError>(TValue? Value, TError? Error, bool IsSuccess)
{
    public bool IsFailure => !IsSuccess;

    public static implicit operator Result<TValue, TError>(TValue value)
    {
        return Result.Success(value);
    }

    public static implicit operator Result<TValue, TError>(Result<TValue, None> result)
    {
        return new Result<TValue, TError>(result.Value, default, true);
    }

    public static implicit operator Result<TValue, TError>(Result<None, TError> result)
    {
        return new Result<TValue, TError>(default, result.Error, false);
    }
}

public struct None
{
}

public static class Result
{
    public static Result<TValue, None> Success<TValue>(TValue value)
    {
        return new Result<TValue, None>(value, default, true);
    }

    public static Result<None, TError> Error<TError>(TError value)
    {
        return new Result<None, TError>(default, value, false);
    }
}