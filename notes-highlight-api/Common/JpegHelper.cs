namespace notes_highlight_api.Common;

public static class JpegHelper
{
    private static readonly int?[] Header = [255, 216, 255, 224, null, null, 74, 70, 73, 70, 0];

    public static bool CheckFile(byte[] image)
    {
        if (image.Length < 11)
            return false;

        for (var i = 0; i < 11; i++)
        {
            if (Header[i] is not null && image[i] != Header[i])
            {
                return false;
            }
        }

        return true;
    }
}