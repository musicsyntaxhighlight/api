using System.Text.Json;
using Amazon.Lambda.APIGatewayEvents;
using Yandex.Cloud.Functions;

namespace notes_highlight_api;

public class Handler : YcFunction<YandexGatewayProxyRequest, YandexGatewayProxyResponse>
{
    public YandexFunction? Function;
    
    public YandexGatewayProxyResponse FunctionHandler(YandexGatewayProxyRequest request, Context context)
    {
        Function ??= new YandexFunction();
        Console.WriteLine(JsonSerializer.Serialize(request));
        return Function.FunctionHandler(request).GetAwaiter().GetResult();
    }
}