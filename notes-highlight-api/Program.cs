using System.Text.Json.Serialization;
using notes_highlight_api;
using notes_highlight_api.Handlers.DAL;
using notes_highlight_api.Repository.Models;

var builder = WebApplication.CreateBuilder(args);
var startup = new Startup();

startup.ConfigureServices(builder.Services);
var app = builder.Build();
startup.Configure(app, null!);
app.Run();
Console.WriteLine(new Handler());

namespace notes_highlight_api
{
    [JsonSerializable(typeof(ImageHighlightRequest[]))]
    internal partial class AppJsonSerializerContext : JsonSerializerContext
    {
    }
}