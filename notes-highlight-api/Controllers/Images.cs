using Microsoft.AspNetCore.Mvc;
using notes_highlight_api.Common;
using notes_highlight_api.Common.Results;
using notes_highlight_api.Services.ImageHighlight;

namespace notes_highlight_api.Controllers;

[Route("/api/images")]
public class Images(IImageHighlightService service) : Controller
{
    private static readonly HttpClient SharedClient = new()
    {
        BaseAddress = new Uri("https://functions.yandexcloud.net/d4er9c2m8db0ngqjvbu6"),
    };
    [HttpPost("/api/image/{id:guid}")]
    public async Task<IActionResult> CreateHighlightingProcess([FromRoute] Guid id, IFormFile image)
    {
        var stream = image.OpenReadStream();
        var imageBytes = new byte[stream.Length];
        _ = await stream.ReadAsync(imageBytes);
        
        if (!JpegHelper.CheckFile(imageBytes))
            return BadRequest("Provided file is not jpeg");
        
        var result = await service.CreateHighLightRequest(id, imageBytes);
        _ = await SharedClient.PostAsync("?integration=raw", new StringContent(""));
        if (result is { IsFailure: true, Error: DefaultErrors.Conflict })
            return Conflict();
        
        return Ok(result.Value);
    }

    [HttpGet("/api/image/{id:guid}")]
    public async Task<IActionResult> CheckHighlightStatus([FromRoute] Guid id)
    {
        var result = await service.GetProcess(id);
        if (result is null)
            return NotFound();
        return Ok(result);
    }
}