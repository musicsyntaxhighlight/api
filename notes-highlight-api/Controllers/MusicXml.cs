using System.Net;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace notes_highlight_api.Controllers;

[Route("/api/musicxml")]
public class MusicXml : ControllerBase
{
    private static readonly HttpClient SharedClient = new()
    {
        BaseAddress = new Uri("https://functions.yandexcloud.net/"),
    };

    [HttpPost("/api/musicxml")]
    public async Task<IActionResult> Colorize(IFormFile musicxml)
    {
        await using var content = musicxml.OpenReadStream();
        var buffer = new byte[content.Length];
        var readAsync = await content.ReadAsync(buffer);
        content.Close();
        var stringContent = Encoding.UTF8.GetString(buffer);
        var functionResult = await SharedClient.PostAsync("d4eud2if82r7kes4b3h5", new StringContent(stringContent));
        if (functionResult.StatusCode == HttpStatusCode.BadRequest)
        {
            return BadRequest("Файл не является валидным musicxml");
        }

        functionResult.EnsureSuccessStatusCode();
        var text = await functionResult.Content.ReadAsStreamAsync();
        return File(text, "application/octet-stream", musicxml.FileName);
    }
}