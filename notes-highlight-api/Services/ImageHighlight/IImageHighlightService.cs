using FluentResults;
using notes_highlight_api.Common.Results;
using notes_highlight_api.Services.ImageHighlight.Models;

namespace notes_highlight_api.Services.ImageHighlight;

public interface IImageHighlightService
{
    public Task<Result<HighlightProcessDto, DefaultErrors>> CreateHighLightRequest(Guid processId, byte[] image);
    public Task<HighlightProcessDto?> GetProcess(Guid processId);
}