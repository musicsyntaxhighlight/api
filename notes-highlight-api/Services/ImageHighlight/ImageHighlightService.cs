using notes_highlight_api.Common.Results;
using notes_highlight_api.Repository;
using notes_highlight_api.Repository.Models;
using notes_highlight_api.Services.ImageHighlight.Models;

namespace notes_highlight_api.Services.ImageHighlight;

public class ImageHighlightService(
    IImageRepository imageRepository,
    IProcessRepository processRepository,
    IRequestQueueProducer queueProducer)
    : IImageHighlightService
{
    public async Task<Result<HighlightProcessDto, DefaultErrors>> CreateHighLightRequest(Guid processId, byte[] image)
    {
        var link = await imageRepository.LoadImage($"{processId}_base", image);
        var dto = new HighlightProcessDto(processId, ImageProcessingStatus.Created, link, null);
        var get = await GetProcess(processId);
        if (get is not null)
        {
            return Result.Error(DefaultErrors.Conflict);
        }

        await processRepository.CreateOrUpdate(dto);
        await queueProducer.CreateRequest(new ImageHighlightRequest(Guid.NewGuid(), dto.ProcessId, dto.BaseImageLink));
        return dto;
    }

    public async Task<HighlightProcessDto?> GetProcess(Guid processId)
    {
        return await processRepository.Get(processId);
    }
}