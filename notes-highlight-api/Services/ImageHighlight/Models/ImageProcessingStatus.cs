namespace notes_highlight_api.Services.ImageHighlight.Models;

public enum ImageProcessingStatus
{
    Unknown = 0,
    Created = 1,
    Processing = 2,
    Ready = 3
}