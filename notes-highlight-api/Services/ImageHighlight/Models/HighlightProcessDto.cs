namespace notes_highlight_api.Services.ImageHighlight.Models;

public record HighlightProcessDto(
    Guid ProcessId,
    ImageProcessingStatus Status,
    string BaseImageLink,
    string? ProcessedImageLink
);