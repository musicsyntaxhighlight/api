using System.Text;
using AspNetCore.Yandex.ObjectStorage;
using Microsoft.OpenApi.Models;
using notes_highlight_api.Handlers;
using notes_highlight_api.Handlers.DAL;
using notes_highlight_api.Repository;
using notes_highlight_api.Repository.Models;
using notes_highlight_api.Services.ImageHighlight;

namespace notes_highlight_api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureHttpJsonOptions(options =>
            {
                options.SerializerOptions.TypeInfoResolverChain.Insert(0, AppJsonSerializerContext.Default);
            });
            services.AddEndpointsApiExplorer();
            services.AddControllers();
            services.AddSwaggerGen();
            services.AddSingleton<Configuration, Configuration>();
            services.AddSingleton<IYdbScheme, YdbScheme>();
            services.AddSingleton<IYdbClient, YdbClient>();
            services.AddSingleton<YdbSerializerHelper<HighlightProcessDbo>, YdbSerializerHelper<HighlightProcessDbo>>();
            services.AddScoped<IYandexStorageFabric, YandexStorageFabric>();
            services.AddScoped<IRequestQueueProducer, RequestQueueProducer>();
            services.AddScoped<IImageRepository, ImageRepository>();
            services.AddScoped<IProcessRepository, ProcessRepository>();
            services.AddScoped<IHighlightProcessHandler, HighlightProcessHandler>();
            services.AddScoped<IImageHighlightService, ImageHighlightService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            app.UseSwagger();
            app.UseSwaggerUI();
        }
    }
}