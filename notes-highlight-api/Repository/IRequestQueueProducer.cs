using System.Diagnostics.CodeAnalysis;
using notes_highlight_api.Repository.Models;

namespace notes_highlight_api.Repository;

public interface IRequestQueueProducer
{
    public Task CreateRequest(ImageHighlightRequest request);
}