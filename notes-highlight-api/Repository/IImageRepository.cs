namespace notes_highlight_api.Repository;

public interface IImageRepository
{
    public Task<string> LoadImage(string imageName, byte[] image);
    public byte[] GetImage(string imageName);
}