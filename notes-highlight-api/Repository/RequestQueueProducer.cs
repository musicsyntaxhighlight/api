using System.Reflection;
using System.Text.Json;
using Amazon;
using Amazon.Runtime.Internal;
using Amazon.SQS;
using Amazon.SQS.Model;
using notes_highlight_api.Handlers.DAL;
using notes_highlight_api.Repository.Models;

namespace notes_highlight_api.Repository;

public class RequestQueueProducer(Configuration configuration) : IRequestQueueProducer
{
    public async Task CreateRequest(ImageHighlightRequest request)
    {
        var config = new AmazonSQSConfig()
        {
            ServiceURL = "https://message-queue.api.cloud.yandex.net"
        };
        config.ServiceURL = "https://message-queue.api.cloud.yandex.net";
        config.AuthenticationRegion = "ru-central1";
        var sqsClient = new AmazonSQSClient(
            config
        );
        var queueRequest = new SendMessageRequest
        {
            QueueUrl = configuration.QueueUrl,
            MessageBody = JsonSerializer.Serialize(request,typeof(ImageHighlightRequest), AppJsonSerializerContext.Default)
        };
        
        var response = await sqsClient.SendMessageAsync(queueRequest);
        
        Console.WriteLine($"Message sent with MessageId: {response.MessageId}");
    }
}