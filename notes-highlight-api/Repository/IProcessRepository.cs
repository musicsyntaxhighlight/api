using notes_highlight_api.Services.ImageHighlight.Models;

namespace notes_highlight_api.Repository;

public interface IProcessRepository
{
    public Task CreateOrUpdate(HighlightProcessDto processDto);
    public Task<HighlightProcessDto?> Get(Guid processId);
}