using notes_highlight_api.Handlers;
using notes_highlight_api.Services.ImageHighlight.Models;

namespace notes_highlight_api.Repository;

public class ProcessRepository(IHighlightProcessHandler highlightProcessHandler) : IProcessRepository
{
    public async Task CreateOrUpdate(HighlightProcessDto processDto)
    {
        var dbo = Convert(processDto);
        await highlightProcessHandler.UpdateStatus(dbo);
    }

    public async Task<HighlightProcessDto?> Get(Guid processId)
    {
        var dbo = await highlightProcessHandler.GetStatus(processId);
        return dbo is null ? null : Convert(dbo);
    }

    private static HighlightProcessDto Convert(HighlightProcessDbo dbo)
    {
        return new HighlightProcessDto(dbo.Id, (ImageProcessingStatus) dbo.Status, dbo.BaseImageLink,
            dbo.ReadyImageLink);
    }
    private static HighlightProcessDbo Convert(HighlightProcessDto dbo)
    {
        return new HighlightProcessDbo
        {
            Id = dbo.ProcessId,
            Status = (int) dbo.Status,
            BaseImageLink = dbo.BaseImageLink,
            ReadyImageLink = dbo.ProcessedImageLink,
            Timestamp = 0
        };
    }
}