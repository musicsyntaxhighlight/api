namespace notes_highlight_api.Repository.Models;

public record ImageHighlightRequest(Guid RequestId, Guid ProcessId, string ImageLink);
