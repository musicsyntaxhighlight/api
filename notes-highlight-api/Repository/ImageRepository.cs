using AspNetCore.Yandex.ObjectStorage;
using notes_highlight_api.Handlers.DAL;

namespace notes_highlight_api.Repository;

public class ImageRepository(IYandexStorageFabric fabric, Configuration configuration) : IImageRepository
{
    private readonly YandexStorageService _service = fabric.Create(configuration);

    public async Task<string> LoadImage(string imageName, byte[] image)
    {
        var load = await _service.ObjectService.PutAsync(image, imageName);
        return load.ReadResultAsStringAsync().Result.Value;
    }

    public byte[] GetImage(string imageName)
    {
        throw new NotImplementedException();
    }
}